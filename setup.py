#!/usr/bin/env python
from setuptools import setup

setup(
    name='DjangoLightbox',
    version='0.2.2',
    author='Geert Dekkers',
    author_email='geert@djangowebstudio.nl',
    packages=[
        "lightbox",
        "lightbox.templatetags",
    ],
    scripts=[],
    url='http://pypi.python.org/pypi/DjangoLightbox/',
    license='LICENSE.txt',
    description='alternative for a full screen javascript lightbox for the django framework',
    long_description_content_type='text/plain',
    long_description=open('README.txt').read(),
    install_requires=[
        "Django >= 1.11",
        "sorl-thumbnail >= 12.5.0",
    ],
    package_data={
        'lightbox': ['templates/lightbox/base.html',],
    },
    keywords = "lightbox images image presentation",
)